package fi.vamk.e1701300.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import fi.vamk.e1701300.demo.Attendance;
import fi.vamk.e1701300.demo.AttendanceRepository;



@SpringBootApplication
public class DemoApplication {
    
    @Autowired
	private AttendanceRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	public void initDate(){
		Attendance att = new Attendance("QWERTY");
		Attendance att1 = new Attendance("ABC");
		Attendance att2 = new Attendance("XYZ");
		
		repository.save(att);
		repository.save(att1);
		repository.save(att2);
	}

}
