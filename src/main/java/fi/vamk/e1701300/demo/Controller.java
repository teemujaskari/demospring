package fi.vamk.e1701300.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class Controller{

    @RequestMapping("/test")
    public String test(){
        return "{\"id\":1}";
    }
    //test
}